##### Добавить в манифест output: список имён Хостов
добавляем в `task.tf`
```yaml
output "Host_names_List" {
value = [for instance in google_compute_instance.vm_instance : instance.name]
}
```
##### Добавить в манифест output: ID проекта
1. Выделяем Project ID в переменную. 
2. объявляем ее в файле `variables.tf`
```yaml
variable "projectID" {
  description = "GCP project ID"
  type = string
}
```
3. обновляем `provider.tf`
4. значение указываем в `terraform.tfvars`
5. используем ее в `outputs` в `task.tf`
```yaml
output "Project_ID" {
  description = "Project ID"
  value = var.projectID
}
```
##### Создать в файерволе правило разрешающее:
	tcp 80 - all  
	tcp 433 - all  
	udp 10000-20000 - 10.0.0.23 (только для одного хоста)

Добавляем правила firewall  в `task.tf`
```yaml
resource "google_compute_firewall" "rules_for_tcp" {
  name = "allow-tcp"
  network = "default"
  allow {
    protocol = "tcp"
    ports = ["80", "443"]
  }
source_tags = ["mynetwork"]
}

resource "google_compute_firewall" "allow_udp_10000_20000" {
  name = "allow-udp"
  network = "default"
  source_ranges = ["10.0.0.23"]
  allow {
    protocol = "udp"
    ports = ["10000-20000"]
  }
}
```
проверяем применение правил: `gcloud compute firewall-rules list` чтобы увидеть `sourceRanges` из правил UDP добавить `--format=jason`
