provider "google" {
  credentials = file("less-5-terraform-ff1cc8b5ca2c.json")

  project = var.projectID 
  region  = "asia-northeast1"
  zone    = "asia-northeast1-a"
}
