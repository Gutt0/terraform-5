variable "private_key_path" {
  description = "priv key"
}

variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "vm_instance" {
  count        = var.node_count
  name         = "node${count.index + 1}"
  machine_type = "e2-small"

  connection {
    user        = "admin"
    private_key = file(var.private_key_path)
    timeout     = "2m"
    host        = self.network_interface.0.access_config.0.nat_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y nginx",
      "sudo tee /var/www/html/index.html <<EOF",
      "<html>",
      "<body>",
      "<h1>Juneway ${self.network_interface.0.access_config.0.nat_ip} ${self.name}</h1>",
      "</body>",
      "</html>",
      "EOF",
      "sudo systemctl restart nginx",
    ]
  }

  provisioner "local-exec" {
    command = "echo node${count.index + 1} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      size  = 20
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

output "Host_names_List" {
  value = [for instance in google_compute_instance.vm_instance : instance.name]
}

output "Project_ID" {
  description = "Project ID" 
  value = var.projectID
}

resource "google_compute_firewall" "rulets_for_tcp" {
  name = "allow-tcp"
  network = "default"
  allow {
    protocol = "tcp"
    ports = ["80","443"]
  }
  source_tags = ["mynetwork"]
}

resource "google_compute_firewall" "allow_upd_10000_20000" {
  name = "allow-upd"
  network = "default"
  source_ranges = ["10.0.0.23"]
  allow {
    protocol = "udp"
    ports = ["10000-20000"]
  }
}
